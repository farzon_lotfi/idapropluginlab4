import idaapi
import idautils
#import ida_typeinf
from ida_funcs import *
import string
import sys

registers32bit = {
"eax" :["0",""],
"ebx" :["0",""],
"ecx" :["0",""],
"edx" :["0",""],
"esi" :["0",""],
"edi" :["0",""],
"ebp" :["0",""],
"esp" :["0",""],
"EFLAGS" :["0",""],
"IP" :["0",""]
}

regsWithNoBitnessDep = set(["ebp","esp","EFLAGS", "IP"])

flagsRegister = {
"CF" :False,  # Carry flag
"PF" :False,  # Parity flag
"AF " :False, # Adjust flag
"ZF" :False,  # Zero flag
"SF" :False,  # Sign flag
"TF" :False,  # Trap flag
"IF" :False,  # Interrupt enable flag
"DF" :False,  # Direction flag
"OF" :False,  # Overflow flag
"IOPL": False,# I/O privilege leve
"NT" : False  # Nested task
}

registers16bit = {
"ax"  :["0",""],
"bx"  :["0",""],
"cx"  :["0",""],
"dx"  :["0",""],
"di"  :["0",""],
"si"  :["0",""]
}

registers8bit = {
"ah"  :["0",""],
"al"  :["0",""],
"bh"  :["0",""],
"bl"  :["0",""],
"ch"  :["0",""],
"cl"  :["0",""],
"dh"  :["0",""],
"dl"  :["0",""]
}
shadowStack = []
#{'instruction':{'D': list<Values>, 'U': list<Values>}}

defUseTable = {}

initBlockValue = {
"eax" :"0",
"ebx" :"0",
"ecx" :"0",
"edx" :"0",
"esi" :"0",
"edi" :"0",
"ebp" :"0",
"esp" :"0",
"EFLAGS" :"0",
"IP"  :"0",
"ax"  :"0",
"bx"  :"0",
"cx"  :"0",
"dx"  :"0",
"di"  :"0",
"si"  :"0",
"ah"  :"0",
"al"  :"0",
"bh"  :"0",
"bl"  :"0",
"ch"  :"0",
"cl"  :"0",
"dh"  :"0",
"dl"  :"0"
}

fsthreadRegisters = {}

def isFsthreadRegisters(operand):
    return operand.startswith("large fs:")

def addToFsthreadRegisters(operand, ea):
    if isFsthreadRegisters(operand):
        fsthreadRegisters[ operand[len("large "):]] = customHex(ea)

def getFsThreadRegisters(operand):
    fsEA = fsthreadRegisters.get( operand[len("large "):])
    if(fsEA == None):
        return "0"
    return fsEA

def initBlock():
    return  initBlockValue.copy()

def addToDefUsTable(ea, defArr):
    global defUseTable
    filter(str.strip, defArr)
    strEA =customHex(ea)
    if defUseTable.get(strEA) == None:
        defUseTable[strEA] = defArr
    else:
        defUseTable[strEA] = list(set(defUseTable[strEA] + defArr))

def getDefUseTableAsArr(ea):
    strEA =customHex(ea)
    return defUseTable.get(strEA)


def getDefUseTable(ea):
    strEA =customHex(ea)
    defList = defUseTable.get(strEA)
    if defUseTable.get(strEA) != None:
        return formatEAList(defList)
    return ""


popWarning = None

reachablePaths = {}

blockToDefs = {} # blockToDefs[blockId] -> set Defs
blockToUses = {} # blockToUses[blockId] -> set Uses

eaToComputedValue = {}

def isOperandAMemBracket(operand):
    return (operand[0] == '[' and operand[-1] == ']')

def isOperandAMemOffset(operand):
    return operand.startswith("offset ")

def getOffsetEA(operand):
    return customHex(idc.get_name_ea_simple(operand[len("offset "):]))

def isOperandAMemByte(operand):
    return operand.startswith("byte_")

def isOperandAFuncAddress(operand):
    return (operand.startswith("ds:") or operand.startswith("s") or
            getFuncByName(operand) != None)

def getEAForByte(operand):
    return int("0x" + operand[len("byte_"):],16)

def isOperandAMemDword(operand):
    return operand.startswith("dword_")

def isOperandABytePtr(operand):
    return operand.startswith("byte ptr ")

def isOperandADwordPtr(operand):
    return operand.startswith("dword ptr ")

def stripDwordPtr(operand):
    return operand[len("dword ptr "):]

def stripBytePtr(operand):
    return operand[len("byte ptr "):]

def getEAForDword(operand):
    return int("0x" + operand[len("dword_"):],16)

def customHex(num):
    return format(num,'x')

def formatEAList(list):
    return ','.join(str(x) for x in list)


def addToEAtoComputedValue(ea, computedValue):
    global eaToComputedValue;
    if eaToComputedValue.get(ea) == None:
        eaToComputedValue[ea]= set([computedValue]);
    else:
        eaToComputedValue[ea].add(computedValue)
    return eaToComputedValue[ea]

def addToDefUseChain(blockId, defArr, useArr, ea):
    if blockToDefs.get(blockId) == None:
        blockToDefs[blockId] = {}
    eaStr = customHex(ea)
    for define in defArr:
        blockToDefs[blockId][define] = eaStr
        if isRegisterWithDep(define):
            regList = []
            if define in registers8bit.keys():
                regList = updateRegDep8bit(define, eaStr)
            else:
                regList = updateRegDep16or32bit(define, eaStr)
            for reg in regList:
                blockToDefs[blockId][reg] = eaStr

    if blockToUses.get(blockId) == None:
        blockToUses[blockId] = {}
    for use in useArr:
        blockToUses[blockId][use] = blockToDefs[blockId][define]

def iterateBlockDefs(operand, blockDefs):
    eaList = []
    for blockDef in blockDefs:
        ea = blockDef.get(operand)
        if(ea != None):
            eaList.append(ea)
    return eaList

def updateBlockDefs(operand, blockDefs, ea):
    if len(blockDefs) == 0:
        blockDefs.append( {operand : setInstStr.format(customHex(ea))})
    else:
        for blockDef in blockDefs:
            updateDep(operand, ea, blockDef)

setInstStr = "{}"

class duplugin_t(idaapi.plugin_t):
    flags = idaapi.PLUGIN_UNL
    comment = "This plugin finds DU chains"
    help = "if you need help, your doing it wrong."
    wanted_name = "DU chain builder"
    wanted_hotkey = "Alt-F9"
    
    def init(self):
        return idaapi.PLUGIN_OK
    
    def run(self, arg):
        print "Start"
        getGlobals()
        #genReachableBasicBlocks(getWinMainFunction(), False)
        runOnAllFuncs()
    def term(self):
        pass

def PLUGIN_ENTRY():
    return duplugin_t()

def getFuncByName(searchFuncName):
    for i in range(0,get_func_qty()):
        func = getn_func(i)
        funcName = get_func_name2(func.startEA)
        if funcName == searchFuncName:
            return func
    return None

def getWinMainFunction():
    #return  getFuncByName("sub_40101C")
    #return getFuncByName("sub_401406")
    #return getFuncByName("StartAddress")
    #return getFuncByName("main");
    return getFuncByName("_WinMain@16")

def runOnAllFuncs():
    global defUseTable
    global popWarning
    for i in range(0,get_func_qty()):
        func = getn_func(i)
        funcName =  get_func_name2(func.startEA)
        print funcName
        if funcName == "sub_401406" or funcName == "StartAddress":
            genReachableBasicBlocks(func,False)
            continue
        if(popWarning != None):
            print "warning pop at {} in block {} tried to pop when empty".format(popWarning[0],
                                                                                 popWarning[1])
            popWarning = None
        genReachableBasicBlocks(func)


def isRegister(operand):
    return (operand in registers32bit.keys() or
    operand in registers16bit.keys() or
    operand in registers8bit.keys())

def isRegisterWithDep(operand):
    return isRegister(operand) and  operand not in regsWithNoBitnessDep

def getRegisterValue(operand):
    if operand in registers32bit.keys():
       return registers32bit[operand]
    elif operand in registers16bit.keys():
       return registers16bit[operand];
    elif operand in registers8bit.keys():
        return registers8bit[operand];
    else:
        return["",""]

def getGlobals():
    # get segment start and end EA by name
    idata_seg_selector = idc.SegByName('.data')
    idata_seg_startea = idc.SegByBase(idata_seg_selector)
    idata_seg_endea = idc.SegEnd(idata_seg_startea)

    # iterate EAs in range
    for seg_ea in range(idata_seg_startea, idata_seg_endea):
        # iterate xrefs to specific ea
        for xref in idautils.XrefsTo(seg_ea):
            if xref.type == 2: #data write:
                addToDefUsTable(seg_ea, [ customHex(xref.frm) ])
    
        for xref in idautils.XrefsTo(seg_ea):
            if xref.type == 3: #data read:
                eaList = getDefUseTableAsArr(seg_ea)
                if eaList != None:
                    addToDefUsTable(xref.frm, eaList)
    #print getDefUseTableAsArr(0x405910)
    #print getDefUseTableAsArr(0x402d61)
    #print getDefUseTableAsArr(0x405918)
    #print getDefUseTableAsArr(0x402D71)
    #for name in idautils.Names():
    #    print name

def updateDep(operand, ea, blockDef):
    eaStr = setInstStr.format(customHex(ea))
    if isRegisterWithDep(operand):
        regList = []
        if operand in registers8bit.keys():
            regList = updateRegDep8bit(operand, eaStr)
        else:
            regList = updateRegDep16or32bit(operand, eaStr)
        for reg in regList:
            blockDef[reg] = eaStr
            #print "reg {}= ea: {}".format(reg,eaStr)
    elif operand in regsWithNoBitnessDep:
        registers32bit[operand][0] = eaStr
        blockDef[operand] = eaStr
    else:
        blockDef[operand] = eaStr


def updateRegDep8bit(operand, eaStr):
    if(operand == "al"):
        registers32bit["eax"][0] = eaStr
        registers16bit["ax"][0] = eaStr
        registers8bit["al"][0] = eaStr
        return ["eax","ax", "al"];
    elif(operand == "ah"):
        registers32bit["eax"][0] = eaStr
        registers16bit["ax"][0] = eaStr
        registers8bit["ah"][0] = eaStr
        return ["eax","ax", "ah"];
    elif(operand == "bl"):
        registers32bit["ebx"][0] = eaStr
        registers16bit["bx"][0] = eaStr
        registers8bit["bl"][0] = eaStr
        return ["ebx","bx", "bl"];
    elif(operand == "bh"):
        registers32bit["ebx"][0] = eaStr
        registers16bit["bx"][0] = eaStr
        registers8bit["bh"][0] = eaStr
        return ["ebx","bx", "bh"];
    elif(operand == "cl"):
        registers32bit["ecx"][0] = eaStr
        registers16bit["cx"][0] = eaStr
        registers8bit["cl"][0] = eaStr
        return ["ecx","cx", "cl"];
    elif(operand == "ch"):
        registers32bit["ecx"][0] = eaStr
        registers16bit["cx"][0] = eaStr
        registers8bit["ch"][0] = eaStr
        return ["ecx","cx", "ch"];
    elif(operand == "dl"):
        registers32bit["edx"][0] = eaStr
        registers16bit["dx"][0] = eaStr
        registers8bit["dl"][0] = eaStr
        return ["edx","dx", "dl"];
    elif(operand == "dh"):
        registers32bit["edx"][0] = eaStr
        registers16bit["dx"][0] = eaStr
        registers8bit["dh"][0] = eaStr
        return ["edx","dx", "dh"];
    else:
        return []


def updateRegDep16or32bit(operand, eaStr):
    if(operand == "eax" or operand == "ax"):
        registers32bit["eax"][0] = eaStr
        registers16bit["ax"][0] = eaStr
        registers8bit["al"][0] = eaStr
        registers8bit["ah"][0] = eaStr
        return ["eax","ax", "al","ah"];
    elif(operand == "ebx" or operand == "bx"):
        registers32bit["ebx"][0] = eaStr
        registers16bit["bx"][0] = eaStr
        registers8bit["bl"][0] = eaStr
        registers8bit["bh"][0] = eaStr
        return ["ebx","bx", "bl","bh"];
    elif(operand == "ecx" or operand == "cx"):
        registers32bit["ecx"][0] = eaStr
        registers16bit["cx"][0] = eaStr
        registers8bit["cl"][0] = eaStr
        registers8bit["ch"][0] = eaStr
        return ["ecx","cx", "cl","ch"];
    elif(operand == "edx" or operand == "dx"):
        registers32bit["edx"][0] = eaStr
        registers16bit["dx"][0] = eaStr
        registers8bit["dl"][0] = eaStr
        registers8bit["dh"][0] = eaStr
        return ["edx","dx", "dl","dh"];
    elif(operand == "edi" or operand == "di"):
        registers32bit["edi"][0] = eaStr
        registers16bit["di"][0] = eaStr
        return ["edi","di"];
    elif(operand == "esi" or operand == "si"):
        registers32bit["esi"][0] = eaStr
        registers16bit["si"][0] = eaStr
        return ["esi","si"];
    else:
        print "op: {} at: {}".format(operand, eaStr)
        return []

def updateReg32bitValue(operand, value):
    num =0
    if(isDigit(value[1])):
        num = genDigit(value[1])
    if(operand == "eax"):
        registers16bit["ax"][0] = value[0]
        registers8bit["al"][0] = value[0]
        registers8bit["ah"][0] = value[0]
        registers32bit["eax"] = value
        registers16bit["ax"][1] = str(num & 0xffff)
        registers8bit["al"][1] = str(num & 0xff)
        registers8bit["ah"][1] = str(num & 0xff00)
    elif(operand == "ebx"):
        registers16bit["bx"][0] = value[0]
        registers8bit["bl"][0] = value[0]
        registers8bit["bh"][0] = value[0]
        registers32bit["ebx"] = value
        registers16bit["bx"][1] = str(num & 0xffff)
        registers8bit["bl"][1] = str(num & 0xff)
        registers8bit["bh"][1] = str(num & 0xff00)
    elif(operand == "ecx"):
        registers16bit["cx"][0] = value[0]
        registers8bit["cl"][0] = value[0]
        registers8bit["ch"][0] = value[0]
        registers32bit["ecx"] = value
        registers16bit["cx"][1] = str(num & 0xffff)
        registers8bit["cl"][1] = str(num & 0xff)
        registers8bit["ch"][1] = str(num & 0xff00)
    elif(operand == "edx"):
        registers16bit["dx"][0] = value[0]
        registers8bit["dl"][0] = value[0]
        registers8bit["dh"][0] = value[0]
        registers32bit["edx"] = value
        registers16bit["dx"][1] = str(num & 0xffff)
        registers8bit["dl"][1] = str(num & 0xff)
        registers8bit["dh"][1] = str(num & 0xff00)
    elif(operand == "edi"):
        registers16bit["di"][0] = value[0]
        registers32bit["edi"] = value
        registers16bit["di"][1] = str(num & 0xffff)
    elif(operand == "esi"):
        registers16bit["si"][0] = value[0]
        registers32bit["esi"] = value
        registers16bit["si"][1] = str(num & 0xffff)

def set32BitRegWith16BitValue(operand32bit, num):
    eaxValue = registers32bit[operand32bit][1]
    eaxNum = 0
    if(isDigit(eaxValue)):
        eaxNum = genDigit(eaxValue)
        eaxNum = eaxNum & 0xffff0000
        #                   4 3 2 1
        registers32bit[operand32bit][1] = str(eaxNum | (num & 0xffff))

def updateReg16bitValue(operand, value):
    num =0
    if(isDigit(value[1])):
        num = genDigit(value[1])
    if(operand == "ax"):
        set32BitRegWith16BitValue("eax", num)
        registers32bit["eax"][0] = value[0]
        registers16bit["ax"][0] = value[0]
        registers8bit["al"][0] = value[0]
        registers8bit["ah"][0] = value[0]
        registers16bit["ax"][1] = str(num & 0xffff)
        registers8bit["al"][1] = str(num & 0xff)
        registers8bit["ah"][1] = str(num & 0xff00)
    elif(operand == "bx"):
        set32BitRegWith16BitValue("ebx", num)
        registers32bit["ebx"][0] = value[0]
        registers16bit["bx"][0] = value[0]
        registers8bit["bl"][0] = value[0]
        registers8bit["bh"][0] = value[0]
        registers16bit["bx"][1] = num & 0xffff
        registers8bit["bl"][1] = num & 0xff
        registers8bit["bh"][1] = num & 0xff00
    elif(operand == "cx"):
        set32BitRegWith16BitValue("ecx", num)
        registers32bit["ecx"][0] = value[0]
        registers16bit["cx"][0] = value[0]
        registers8bit["cl"][0] = value[0]
        registers8bit["ch"][0] = value[0]
        registers16bit["cx"][1] = str(num & 0xffff)
        registers8bit["cl"][1] = str(num & 0xff)
        registers8bit["ch"][1] = str(num & 0xff00)
    elif(operand == "dx"):
        set32BitRegWith16BitValue("edx", num)
        registers32bit["edx"][0] = value[0]
        registers16bit["dx"][0]  = value[0]
        registers8bit["dl"][0]  = value[0]
        registers8bit["dh"][0]  = value[0]
        registers16bit["dx"][1] = str(num & 0xffff)
        registers8bit["dl"][1] = str(num & 0xff)
        registers8bit["dh"][1] = str(num & 0xff00)
        

def setRegisterValue(operand, value):
    if operand in registers32bit.keys():
        updateReg32bitValue(operand, value)
    elif operand in registers16bit.keys():
        updateReg16bitValue(operand, value)
    elif operand in registers8bit.keys():
        registers8bit[operand] = value; #TODO


def initRegisterValues():
    for reg in registers32bit.keys():
        setRegisterValue(reg, ["0",""])
    for reg in registers16bit.keys():
        setRegisterValue(reg, ["0",""])
    for reg in registers8bit.keys():
        setRegisterValue(reg, ["0",""])
    for reg in regsWithNoBitnessDep:
        registers32bit[reg] = ["0",""]
    for flag in flagsRegister.keys():
        flagsRegister[flag] = False
    #print registers32bit
    #print registers16bit
    #print registers8bit


def isDigit(operand):
    return (operand.isdigit() or
            (len(operand) > 0 and operand[-1] == 'h' and
             all(c in string.hexdigits for c in operand[:-1])))

def genDigit(operand):
    if(operand[-1] == 'h'):
        return int(operand[:-1], 16)
    return int(operand, 16)

def initRegisterNode(ea):
    return [setInstStr.format(customHex(ea)), ""]

def noDepComment(ea):
    MakeComm(ea, "no dependency");

def getMemOps(operand):
    mempOps = operand[1:-1]
    return mempOps.replace('+',' ').replace('-',' ').split()

def commentPushInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "push"):
        operand = idc.GetOpnd(ea, 0)
        eaList1 = []
        useList = ["esp"]
        if not isDigit(operand):
            useList.append(operand)
            eaList1 = iterateBlockDefs(operand, blockDefs)
        if isOperandAMemBracket(operand):
            mempOpsArr = getMemOps(operand)
            for op in mempOpsArr:
                if not isDigit(op):
                    useList.append(op)
                    eaList1 = eaList1 + iterateBlockDefs(op, blockDefs)
        elif isOperandAMemOffset(operand):
            eaList1.append(getOffsetEA(operand))
        
        addToDefUseChain(blockId, ["esp", "[esp]"], useList, ea)
        filter(str.strip, eaList1)
        eaList2 = iterateBlockDefs("esp", blockDefs)
        eaList = eaList1 + eaList2
        addToDefUsTable(ea, eaList)

        updateBlockDefs("esp", blockDefs, ea)
        updateBlockDefs("[esp]", blockDefs, ea)
        stackNode = initRegisterNode(ea)
        if(isDigit(operand)):
            stackNode[1] = "{}".format(genDigit(operand))
        if(isRegister(operand)):
            registerNode = getRegisterValue(operand)
            stackNode[1] = "{}".format(registerNode[1]);
        shadowStack.append(stackNode);
        registers32bit["esp"] = stackNode[:]
        #print shadowStack

def commentPopInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "pop"):
        operand = idc.GetOpnd(ea, 0)
        popVal = None
        if(len(shadowStack) == 0):
            global popWarning
            popWarning = [customHex(ea),blockId]
        else:
            popVal = shadowStack.pop()
            #print shadowStack
            registers32bit["esp"] = shadowStack[-1][:] if len(shadowStack) > 0 else ["",""]
            popVals = addToEAtoComputedValue(ea, popVal[1])
            if isRegister(operand):
                registers32bit[operand] = popVal

        eaList = iterateBlockDefs("esp", blockDefs)
        if(popVal !=None):
            eaList.append(popVal[0])
            addToDefUsTable(ea, eaList)
        else:
            addToDefUsTable(ea, eaList)

        addToDefUseChain(blockId, ["esp", operand], ["esp", "[esp]"], ea)
        updateBlockDefs(operand, blockDefs, ea)
        updateBlockDefs("IP", blockDefs, ea)

def strlenRule(ea, operand):
    if(operand == "strlen"):
        nextEa = idc.NextHead(ea)
        opcode = idc.GetMnem(nextEa)
        if(opcode == "pop"):
            return True
        elif(opcode == "cmp" or opcode == "test"):
            nextEa = idc.NextHead(ea)
            opcode = idc.GetMnem(nextEa)
            return opcode == "pop"
    return False

def addRule(ea):
    nextEa = idc.NextHead(ea)
    opcode = idc.GetMnem(nextEa)
    operand = idc.GetOpnd(nextEa, 0)
    return opcode == "add" and operand == "esp"


def commentCallInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "call"):
        operand = idc.GetOpnd(ea, 0)
        funcArgsArr = idaapi.get_arg_addrs(ea)
        if funcArgsArr != None:
            funcArgEaList =  map(lambda x: customHex(x), funcArgsArr)
            addToDefUsTable(ea, funcArgEaList)
            #print "function {} has {} args".format(operand,len(funcArgEaList))
            if addRule(ea) and operand != "??3@YAXPAX@Z" and not strlenRule(ea, operand):
                for i in range(len(funcArgsArr)):
                    shadowStack.pop()
                    #print shadowStack
        else:
            addToDefUsTable(ea, ["(void)"])
        if idaapi.func_does_return(ea):
            operandDef ="eax"
            
            addToDefUseChain(blockId, [operandDef], [], ea)
            updateBlockDefs(operandDef, blockDefs, ea)
            updateBlockDefs("IP", blockDefs, ea)

def andZerolDigitRule(node, operand2):
    if operand2 == "0" or (isinstance(operand2, list) and operand2[1] == "0"):
        node[1] = "0"

def commentAndInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "and"):
        applyAndInst = lambda a, b : "{}".format(genDigit(a) & genDigit(b))
        commentTwoOperandInstruction(opcode, ea, blockId, blockDefs, applyAndInst, andZerolDigitRule)

def commentOrInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "or"):
        applyOrInst = lambda a, b : "{}".format(genDigit(a) | genDigit(b))
        commentTwoOperandInstruction(opcode, ea, blockId, blockDefs, applyOrInst)

def commentSubInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "sub"):
        applySubInst = lambda a, b : "{}".format(genDigit(a) - genDigit(b))
        commentTwoOperandInstruction(opcode, ea, blockId, blockDefs, applySubInst)

def commentAddInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "add"):
        applyAddInst = lambda a, b : "{}".format(genDigit(a) + genDigit(b))
        commentTwoOperandInstruction(opcode, ea, blockId, blockDefs, applyAddInst)
        # Note adding to esp should pop off the shadow stack but its easier to handle in
        # call instruction

def xorZerolRule(node, operand1, operand2):
    if(operand1 == operand2):
        node[1] = "0"

def commentXORInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "xor"):
        applyXorInst = lambda a, b : "{}".format(genDigit(a) ^ genDigit(b))
        commentTwoOperandInstruction(opcode, ea, blockId, blockDefs, applyXorInst, xorZerolRule)

def commentTwoOperandInstruction(opcode, ea, blockId, blockDefs, applyInstFunc, applySpecialRule=None):
    operand1 = idc.GetOpnd(ea, 0)
    operand2 = idc.GetOpnd(ea, 1)
    registerNode = initRegisterNode(ea)
    isOp1BytePtr1 = isOperandABytePtr(operand1)
    isOp1BytePtr2 = isOperandABytePtr(operand2)
    isOp1DwordPtr1 = isOperandADwordPtr(operand1)
    isOp1DwordPtr2 = isOperandADwordPtr(operand2)
    
    useList = []
    defArr = ["EFLAGS", operand1]
    useArr = [operand1, operand2]
    if isRegister(operand1):
        
        registerNode1 = getRegisterValue(operand1)
        #print "operand1 {}  at {} is a register {}".format(operand1, ea, registerNode1[0])
        useList = useList + iterateBlockDefs(operand1, blockDefs)
        if isRegister(operand2):
            useList = useList + iterateBlockDefs(operand2, blockDefs)
            registerNode2 = getRegisterValue(operand2)
            #print "operand2 {}  at {} is a register {}".format(operand2, ea, registerNode2[0])
            if not isDigit(registerNode1[1]) and applySpecialRule != None:
                if opcode == "xor":
                    applySpecialRule(registerNode, operand1, operand2)
                else:
                    applySpecialRule(registerNode, registerNode2)
            elif( isDigit(registerNode1[1]) and isDigit(registerNode2[1])):
                registerNode[1] = applyInstFunc(registerNode1[1], registerNode2[1])
        elif isDigit(operand2):
            #print "operand2 {} at {} is a digit".format(operand2,ea)
            if isDigit(registerNode1[1]):
                registerNode[1] =  applyInstFunc(registerNode1[1], operand2)
            elif applySpecialRule != None:
                if opcode == "xor":
                    applySpecialRule(registerNode, registerNode1[1], operand2)
                else:
                    applySpecialRule(registerNode, operand2)
        elif isOperandAMemByte(operand2):
            memUses = getDefUseTableAsArr(getEAForByte(operand))
            if(memUses != None):
                useList = useList + memUses
        elif isOperandAMemDword(operand2):
            memUses = getDefUseTableAsArr(getEAForDword(operand2))
            if(memUses != None):
                useList = useList + memUses
        elif isOp1BytePtr2 or isOp1DwordPtr2 or isOperandAMemBracket(operand2):
            memOp1 = operand2;
            if(isOp1BytePtr2):
                memOp1 = stripBytePtr(operand2)
            elif isOp1DwordPtr2:
                memOp1 = stripDwordPtr(operand2)
            mempOpsArr = getMemOps(memOp1)
            for op in mempOpsArr:
                useArr.append(op)
                useList = useList + iterateBlockDefs(op, blockDefs)
        setRegisterValue(operand1, registerNode)
    else: #memory case
        if isRegister(operand2):
            useList = useList + iterateBlockDefs(operand2, blockDefs)

    if isOperandAMemByte(operand1):
        memUses = getDefUseTableAsArr(getEAForByte(operand1))
        if(memUses != None):
            useList = useList + memUses
    elif isOperandAMemDword(operand1):
        memUses = getDefUseTableAsArr(getEAForDword(operand1))
        if(memUses != None):
            useList = useList + memUses
    elif isOp1BytePtr1 or isOp1DwordPtr1 or isOperandAMemBracket(operand1):
        memOp1 = operand1;
        if(isOp1BytePtr1):
            memOp1 = stripBytePtr(operand1)
        elif isOp1DwordPtr1:
            memOp1 = stripDwordPtr(operand1)
        mempOpsArr = getMemOps(memOp1)
        for op in mempOpsArr:
            useArr.append(op)
            useList = useList + iterateBlockDefs(op, blockDefs)


    addToDefUsTable(ea, useList)
    addToDefUseChain(blockId, defArr, useArr, ea)
    updateBlockDefs(operand1, blockDefs, ea)
    updateBlockDefs("IP", blockDefs, ea)
    updateBlockDefs("EFLAGS", blockDefs, ea)

def commentIncDecInstruction(opcode, ea, blockId, blockDefs, applyInstFunc):
    if(opcode == "inc" or opcode == "dec"):
        operand = idc.GetOpnd(ea, 0)
        isOp1BytePtr = isOperandABytePtr(operand)
        defArr = ["EFLAGS", operand]
        useArr = [operand]
        useList = []
        registerNode = initRegisterNode(ea)
        if isRegister(operand):
            registerNode = getRegisterValue(operand)
            useList = iterateBlockDefs(operand, blockDefs)
            if isDigit(registerNode[1]):
                registerNode[1] = "{}".format(applyInstFunc(registerNode[1]))
            setRegisterValue(operand, registerNode)
        elif isOperandAMemByte(operand):
            memUses = getDefUseTableAsArr(getEAForByte(operand))
            if(memUses != None):
                useList = useList + memUses
        elif isOperandAMemDword(operand):
            memUses = getDefUseTableAsArr(getEAForDword(operand))
            if(memUses != None):
                useList = useList + memUses
        elif isOp1BytePtr or isOperandAMemBracket(operand):
            memOp1 = operand;
            if(isOp1BytePtr):
                memOp1 = stripBytePtr(operand)
            mempOpsArr = getMemOps(memOp1)
            for op in mempOpsArr:
                useArr.append(op)
                useList = useList + iterateBlockDefs(op, blockDefs)
        addToDefUsTable(ea, useList)
        addToDefUseChain(blockId, defArr, useArr, ea)
        updateBlockDefs(operand, blockDefs, ea)
        updateBlockDefs("EFLAGS", blockDefs, ea)
        updateBlockDefs("IP", blockDefs, ea)

def commentIncInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "inc"):
        applyIncInst = lambda a : "{}".format(genDigit(a) + 1)
        commentIncDecInstruction(opcode, ea, blockId, blockDefs, applyIncInst)

def commentDecInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "dec"):
        applyDecInst = lambda a : "{}".format(genDigit(a) - 1)
        commentIncDecInstruction(opcode, ea, blockId, blockDefs, applyDecInst)

#flagsRegister["CF"]
#flagsRegister["OF"]
#flagsRegister["ZF"]
#flagsRegister["AF"]
#flagsRegister["PF"]
#flagsRegister["SF"]
def commentCmpInstruction( opcode, ea, blockId, blockDefs):
    if(opcode == "cmp"):
        applySubInst = lambda a, b : "{}".format(genDigit(a) - genDigit(b))
        commentComparisonInstruction(opcode, ea, blockId, blockDefs, applySubInst)

def commentTestInstruction( opcode, ea, blockId, blockDefs):
    if(opcode == "test"):
        applyAndInst = lambda a, b : "{}".format(genDigit(a) & genDigit(b))
        flagsRegister["CF"] = False
        flagsRegister["OF"] = False
        flagsRegister["AF"] = False
        commentComparisonInstruction(opcode, ea, blockId, blockDefs, applyAndInst)

def commentSarInstruction( opcode, ea, blockId, blockDefs):
    if(opcode == "sar"):
        commentShiftByInstruction(opcode, ea, blockId, blockDefs)

def commentShrInstruction( opcode, ea, blockId, blockDefs):
    if(opcode == "shr"):
        commentShiftByInstruction(opcode, ea, blockId, blockDefs)
    
def commentShlInstruction( opcode, ea, blockId, blockDefs):
    if(opcode == "shl"):
        commentShiftByInstruction(opcode, ea, blockId, blockDefs)

def commentShiftByInstruction( opcode, ea, blockId, blockDefs):
    operand1 = idc.GetOpnd(ea, 0)
    operand2 = idc.GetOpnd(ea, 1)
    defArr = ["EFLAGS", operand1]
    useArr = []
    useList = []
    isOp1BytePtr = isOperandABytePtr(operand1)
    if isRegister(operand1):
        useArr.append(operand1)
    elif isOperandAMemByte(operand1):
        memUses = getDefUseTableAsArr(getEAForByte(operand1))
        if(memUses != None):
            useList = useList + memUses
    elif isOperandAMemDword(operand1):
        memUses = getDefUseTableAsArr(getEAForDword(operand1))
        if(memUses != None):
            useList = useList + memUses
    elif isOp1BytePtr or isOperandAMemBracket(operand1):
        memOp1 = operand1;
        if(isOp1BytePtr):
            memOp1 = stripBytePtr(operand1)
        mempOpsArr = getMemOps(memOp1)
        for op in mempOpsArr:
            useArr.append(op)
    if operand2 in registers8bit: #else its an immediate
        useArr.append(operand2)

    for operandUse in useArr:
        useList = useList + iterateBlockDefs(operandUse, blockDefs)
    addToDefUsTable(ea, useList)
    # For each shift count, the most significant bit of the destination operand is shifted into the CF flag
    addToDefUseChain(blockId, defArr, useArr, ea)
    updateBlockDefs("EFLAGS", blockDefs, ea)
    updateBlockDefs(operand1, blockDefs, ea)
    updateBlockDefs("IP", blockDefs, ea)


def setFlag(flag, condition):
    if condition:
        flagsRegister[flag] = True
    else:
        flagsRegister[flag] = False

def setZF(node):
    setFlag("ZF", lambda node : node[1] == "0")

def setSF(node):
    setFlag("SF", lambda node : node[1][0] == "-")

def setPF(node):
    bits = format(int(node[1],16),"b")
    lenBits = len(bits)
    if(bits[0] == "-"):
        lenBits = lenBits -1
    setFlag("PF", lambda node : lenBits & 1 == 0)

def commentComparisonInstruction(opcode, ea, blockId, blockDefs, applyInstFunc):
    operand1 = idc.GetOpnd(ea, 0)
    operand2 = idc.GetOpnd(ea, 1)
    useList = []
    registerNode = initRegisterNode(ea)
    isOp1BytePtr = isOperandABytePtr(operand1)
    isOp1DwordPtr1 = isOperandADwordPtr(operand1)
    if isRegister(operand1):
        registerNode1 = getRegisterValue(operand1)
        useList = useList + iterateBlockDefs(operand1, blockDefs)
        if isRegister(operand2):
            registerNode2 = getRegisterValue(operand2)
            useList = useList + iterateBlockDefs(operand2, blockDefs)
            if( isDigit(registerNode1[1]) and isDigit(registerNode2[1])):
                registerNode[1] = applyInstFunc(registerNode1[1], registerNode2[1])
                setZF(registerNode)
                setSF(registerNode)
                setPF(registerNode)
        elif isDigit(operand2):
            if isDigit(registerNode1[1]):
                registerNode[1] =  applyInstFunc(registerNode1[1], operand2)
                setZF(registerNode)
                setSF(registerNode)
                setPF(registerNode)
    elif isOperandAMemByte(operand1):
        memUses = getDefUseTableAsArr(getEAForByte(operand1))
        if(memUses != None):
            useList = useList + memUses
        if isRegister(operand2):
            useList = useList + iterateBlockDefs(operand2, blockDefs)
        #elif isDigit(operand2):
    elif isOperandAMemDword(operand1):
        memUses = getDefUseTableAsArr(getEAForDword(operand1))
        if(memUses != None):
            useList = useList + memUses
        if isRegister(operand2):
            useList = useList + iterateBlockDefs(operand2, blockDefs)
    elif isOp1BytePtr or isOp1DwordPtr1 or isOperandAMemBracket(operand1):
        memOp1 = operand1;
        if(isOp1BytePtr):
            memOp1 = stripBytePtr(operand1)
        elif isOp1DwordPtr1:
            memOP1 = stripDwordPtr(operand1)
        mempOpsArr = getMemOps(memOp1)
        
        for op in mempOpsArr:
            useList = useList + iterateBlockDefs(op, blockDefs)

        if isRegister(operand2):
            useList = useList + iterateBlockDefs(operand2, blockDefs);
        elif isOperandAMemOffset(operand2):
            mempOpsArr = getMemOps(operand2)
            for op in mempOpsArr:
                useList = useList + iterateBlockDefs(op, blockDefs)
    else:
        eaGlobal = idc.get_name_ea_simple(operand1)
        if eaGlobal != 0xffffffffffffffffL:
            useList.append(customHex(eaGlobal))
            useList = useList + iterateBlockDefs(operand2, blockDefs)
            addToDefUsTable(eaGlobal,[customHex(ea)])
    

    addToDefUsTable(ea, useList)
    addToDefUseChain(blockId, ["EFLAGS"], [operand1, operand2], ea)
    updateBlockDefs("EFLAGS", blockDefs, ea)
    updateBlockDefs("IP", blockDefs, ea)

def commentSetnlInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "setnl"):
        operand1 = idc.GetOpnd(ea, 0)
        defArr = [operand1]
        useArr = ["EFLAGS"]
        isOp1BytePtr = isOperandABytePtr(operand1)
        useList = iterateBlockDefs("EFLAGS", blockDefs)
        if isOperandAMemByte(operand1):
            memUses = getDefUseTableAsArr(getEAForByte(operand1))
            if(memUses != None):
                useList = useList + memUses
        elif isOperandAMemDword(operand1):
            memUses = getDefUseTableAsArr(getEAForDword(operand1))
            if(memUses != None):
                useList = useList + memUses
        elif isOp1BytePtr or isOperandAMemBracket(operand1):
            memOp1 = operand1;
            if(isOp1BytePtr):
                memOp1 = stripBytePtr(operand1)
            mempOpsArr = getMemOps(memOp1)
            for op in mempOpsArr:
                useArr.append(op)
                useList = useList + iterateBlockDefs(op, blockDefs)

        addToDefUsTable(ea, useList)
        addToDefUseChain(blockId, defArr, useArr, ea)
        updateBlockDefs(defArr[0], blockDefs, ea)
        updateBlockDefs("IP", blockDefs, ea)

def commentStosdInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "stosd"):
        defArr = ["eax"]
        useArr = ["esi","edi"]
        useList = iterateBlockDefs("edi", blockDefs)
        useList = useList + iterateBlockDefs("esi", blockDefs)
        if idc.GetDisasm(ea) == 'rep stosd':
            useArr.append("ecx")
            useList = useList + iterateBlockDefs("ecx", blockDefs)
            #print "found rep"
        addToDefUsTable(ea, useList)
        addToDefUseChain(blockId, defArr, useArr, ea)
        updateBlockDefs(defArr[0], blockDefs, ea)
        updateBlockDefs("IP", blockDefs, ea)



def jumpConditionalBehavior(ea, blockId, blockDefs):
    operand ="EFLAGS"
    defOperand = "IP"
    eaList = iterateBlockDefs(operand, blockDefs)
    addToDefUsTable(ea, eaList)
    addToDefUseChain(blockId, [defOperand], [operand], ea)
    updateBlockDefs(defOperand, blockDefs, ea)

def commentJZInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jz"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)


def commentJNZInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jnz"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)


def commentJmpInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jmp"):
        defArr = ["IP"]
        operand = idc.GetOpnd(ea, 0)
        if(isOperandAFuncAddress(operand)):
            funcArgsArr = idaapi.get_arg_addrs(ea)
            if funcArgsArr != None:
                funcArgEaList =  map(lambda x: customHex(x), funcArgsArr)
                addToDefUsTable(ea, funcArgEaList)
            else:
                addToDefUsTable(ea, ["(void)"])
            if idaapi.func_does_return(ea):
                defArr.append("eax")
        else:
            addToDefUsTable(ea, ["N\A"])
        addToDefUseChain(blockId, defArr, [], ea)
        for defOperand in defArr:
            updateBlockDefs(defOperand, blockDefs, ea)

def commentJleInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jle"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

def commentJlInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jl"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

def commentJgInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jg"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

def commentJgeInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jge"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

def commentJaInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "ja"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

def commentJbInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jb"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

def commentJnbInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jnb"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

def commentJbeInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "jbe"):
        #operand1 = idc.GetOpnd(ea, 0)
        jumpConditionalBehavior(ea, blockId, blockDefs)

callAgain = False
def sameBlockLoopDetect(opcode, ea, block):
    global callAgain
    if(opcode != "" and opcode[0] == 'j'):
        operand = idc.GetOpnd(ea, 0)
        if (operand.startswith("loc_")):
            jmpEA = idc.get_name_ea_simple(operand)
            if(jmpEA < ea and block.startEA == jmpEA):
                callAgain = True

def commentLeaveInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "leave"):
        defArr = ["esp","ebp"] #Equivalent to mov esp, ebp AND pop ebp
        useArr = ["ebp"]
        registers32bit["esp"] = registers32bit["ebp"]
        registers32bit["ebp"][0] = "0"
        eaList = iterateBlockDefs("esp", blockDefs)
        eaList = eaList + iterateBlockDefs("ebp", blockDefs)
        addToDefUsTable(ea, eaList)
        addToDefUseChain(blockId, defArr, useArr, ea)
        updateBlockDefs(defArr[0], blockDefs, ea)
        updateBlockDefs(defArr[1], blockDefs, ea)


def commentIDivInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "idiv"):
        operand1 = idc.GetOpnd(ea, 0)
        defArr = []
        useArr = []
        if operand1 == "":
            operand1 = idc.GetOpnd(ea, 1)
        
        if operand1 in registers8bit:
            defArr = ["ax"]
            useArr = [operand1, "ax"]
        elif operand1 in registers16bit:
            defArr = ["ax", "dx"]
            useArr = [operand1, "dx", "ax"]
        elif operand1 in registers32bit:
            defArr = ["eax", "edx"]
            useArr = [operand1, "edx","eax"]
        elif isOperandAMemBracket(operand1):
            useArr = [operand1, "edx","eax"]
            mempOpsArr = getMemOps(operand1)
            for op in mempOpsArr:
                if not isDigit(operand2):
                    useArr.append(op)
            defArr = ["eax", "edx"] # if this is a byte, this is wrong
        eaList = []
        for operandUse in useArr:
            eaList = eaList + iterateBlockDefs(operandUse, blockDefs)

        addToDefUsTable(ea, eaList)
        addToDefUseChain(blockId, defArr, useArr, ea)
        for operandDef in defArr: #do this so that 8bit-32bit registers also update
            updateBlockDefs(operandDef, blockDefs, ea)

def commentIMulInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "imul"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        operand3 = idc.GetOpnd(ea, 2)
        defArr = []
        useArr = []
        if(operand2 == None and operand3 == None): # one operand form
            if operand1 in registers8bit:
                defArr = ["ax"]
                useArr = [operand1, "al"]
            elif operand1 in registers16bit:
                defArr = ["ax", "dx"]
                useArr = [operand1, "ax"]
            elif operand1 in registers32bit:
                defArr = ["eax", "edx"]
                useArr = [operand1, "eax"]
            elif isOperandAMemBracket(operand1):
                useArr.append(operand1)
                mempOpsArr = getMemOps(operand1)
                for op in mempOpsArr:
                    if not isDigit(operand2):
                        useArr.append(op)
                defArr = ["eax", "edx"] # if this is a byte, this is wrong
        elif(operand3 == None):
            defArr = [operand1]
            if isRegister(operand2):
                 useArr = [operand1, operand2]
            elif isOperandAMemBracket(operand2):
                useArr = [operand1, operand2]
                mempOpsArr = getMemOps(operand2)
                for op in mempOpsArr:
                    if not isDigit(operand2):
                        useArr.append(op)
            #else #isdigit
        else: #operand3 is an immediate value don't add to use def
            defArr = [operand1]
            if isRegister(operand2):
                useArr = [operand2]
            elif operand2 =="":
                useArr = [operand1]
            elif isOperandAMemBracket(operand2):
                useArr.append(operand2)
                mempOpsArr = getMemOps(operand2)
                for op in mempOpsArr:
                    if not isDigit(operand2):
                        useArr.append(op)
        eaList = []
        for operandUse in useArr:
            eaList = eaList + iterateBlockDefs(operandUse, blockDefs)
        addToDefUsTable(ea, eaList)
        addToDefUseChain(blockId, defArr, useArr, ea)
        for operandDef in defArr: #do this so that 8bit-32bit registers also update
            updateBlockDefs(operandDef, blockDefs, ea)

def commentCdqInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "cdq"):
        eaList = iterateBlockDefs("eax", blockDefs)
        addToDefUsTable(ea, eaList)
        addToDefUseChain(blockId, ["eax","edx"], ["eax"], ea)
        updateBlockDefs("eax", blockDefs, ea) #cascade to ax, al, ah
        updateBlockDefs("edx", blockDefs, ea) #cascade to dx dl dh

def commentRetNInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "retn"):
        operand = idc.GetOpnd(ea, 0)
        eaRet = [shadowStack[-1][0]] if len(shadowStack) > 0 else  []
        if(operand):
            i = genDigit(operand)
            while(len(shadowStack) > 0 and i%4 == 0):
                eaRet = [shadowStack.pop()[0]]
                i = i - 4
        addToDefUsTable(ea, eaRet)
        addToDefUseChain(blockId, ["esp"], ["esp"], ea)
        updateBlockDefs("esp", blockDefs, ea)


def commentLeaInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "lea"):
        commentMemInstruction(opcode, ea, blockId, blockDefs)

def commentMovInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "mov"):
        commentMemInstruction(opcode, ea, blockId, blockDefs)

def commentMovsxInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "movsx"):
        operand1 = idc.GetOpnd(ea, 0)
        if isRegister(operand1):
            commentMemInstruction(opcode, ea, blockId, blockDefs)
        else:
            print "warning: instruction not as expected"

def commentMovzxInstruction(opcode, ea, blockId, blockDefs):
    if(opcode == "movzx"):
        operand1 = idc.GetOpnd(ea, 0)
        if isRegister(operand1):
            commentMemInstruction(opcode, ea, blockId, blockDefs)
        else:
            print "warning: instruction not as expected"


def commentMemInstruction(opcode, ea, blockId, blockDefs):
    if(opcode.startswith("mov") or opcode == "lea"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        defsArr = [operand1]
        useArr = [operand2]
        useList = []
        registerNode = initRegisterNode(ea)
        isOp1BytePtr = isOperandABytePtr(operand1)
        if isRegister(operand2):
            useList = useList + iterateBlockDefs(operand2, blockDefs)
        elif isOperandAMemOffset(operand2):
            useList.append(getOffsetEA(operand2))
        elif isOperandAMemByte(operand2):
            memUses = getDefUseTableAsArr(getEAForByte(operand2))
            if(memUses != None):
                useList = useList + memUses
        elif isOperandAMemDword(operand2):
            memUses = getDefUseTableAsArr(getEAForDword(operand2))
            if(memUses != None):
                useList = useList + memUses
            else:
                useList.append(customHex(idc.get_name_ea_simple(operand2)))
        elif isOperandAFuncAddress(operand2):
            funcName = operand2
            if funcName.startswith("ds:"):
                funcName = funcName[3:]
                useList.append(customHex(idc.get_name_ea_simple(funcName)))
        elif isFsthreadRegisters(operand2):
            useList.append(getFsThreadRegisters(operand2))
        else:
            eaGlobal = idc.get_name_ea_simple(operand2)
            if eaGlobal != 0xffffffffffffffffL:
                useList.append(customHex(eaGlobal))
                useList = useList + iterateBlockDefs(operand2, blockDefs)
                addToDefUsTable(eaGlobal,[customHex(ea)])

        if isRegister(operand1):
            if isRegister(operand2):
                registerNode2 = getRegisterValue(operand2)
                if(isDigit(registerNode2[1])):
                    registerNode[1] = registerNode2[1]
            elif isDigit(operand2):
                registerNode[1] = operand2
                useList.append("N\A")
            elif isOperandAMemBracket(operand2):
                useList = useList + iterateBlockDefs(operand2, blockDefs)
                mempOpsArr = getMemOps(operand2)
                for op in mempOpsArr:
                    if not isDigit(operand2): #isRegister(op):
                        useList = useList + iterateBlockDefs(op, blockDefs)
                        useArr.append(op)
        elif isOperandAMemOffset(operand1):
            useList.append(getOffsetEA(operand1))
        elif isOperandAMemByte(operand1):
            memUses = getDefUseTableAsArr(getEAForByte(operand1))
            if(memUses != None):
                useList = useList + memUses
        elif isOperandAMemDword(operand1):
            memUses = getDefUseTableAsArr(getEAForDword(operand1))
            if(memUses != None):
                useList = useList + memUses
        elif isOp1BytePtr or isOperandAMemBracket(operand1):
            memOp1 = operand1;
            if(isOp1BytePtr):
                memOp1 = stripBytePtr(operand1)
            mempOpsArr = getMemOps(memOp1)
            for op in mempOpsArr:
                useArr.append(op)
                useList = useList + iterateBlockDefs(op, blockDefs)
            if isRegister(operand2) or isOperandAMemOffset(operand2):
                useList = useList + iterateBlockDefs(operand2, blockDefs);
            #elif isDigit(operand2):
        elif isFsthreadRegisters(operand1):
            addToFsthreadRegisters(operand1, ea)
        else:
            eaGlobal = idc.get_name_ea_simple(operand1)
            if eaGlobal != 0xffffffffffffffffL:
                useList.append(customHex(eaGlobal))
                useList = useList + iterateBlockDefs(operand1, blockDefs)
                addToDefUsTable(eaGlobal,[customHex(ea)])

        
        
        addToDefUsTable(ea, useList)
        setRegisterValue(operand1, registerNode)
        addToDefUseChain(blockId, defsArr, useArr, ea)
        updateBlockDefs(operand1, blockDefs, ea)
        updateBlockDefs("IP", blockDefs, ea)

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def WriteEAs(block):
    ea = block.startEA
    global defUseTable
    #print defUseTable
    while ea < block.endEA:
        MakeComm(ea,getDefUseTable(ea))
        ea = idc.NextHead(ea)


def getInstructionsBase(block, blockDefs):
    ea = block.startEA
    while ea < block.endEA:
        opcode = idc.GetMnem(ea)
        commentPushInstruction(opcode, ea, block.id, blockDefs)
        commentPopInstruction( opcode, ea, block.id, blockDefs)
        commentAndInstruction( opcode, ea, block.id, blockDefs)
        commentOrInstruction(  opcode, ea, block.id, blockDefs)
        commentXORInstruction( opcode, ea, block.id, blockDefs)
        commentAddInstruction( opcode, ea, block.id, blockDefs)
        commentSubInstruction( opcode, ea, block.id, blockDefs)
        commentIncInstruction( opcode, ea, block.id, blockDefs)
        commentDecInstruction( opcode, ea, block.id, blockDefs)
        commentCmpInstruction( opcode, ea, block.id, blockDefs)
        commentTestInstruction(opcode, ea, block.id, blockDefs)
        commentJZInstruction(  opcode, ea, block.id, blockDefs)
        commentJNZInstruction( opcode, ea, block.id, blockDefs)
        commentJmpInstruction( opcode, ea, block.id, blockDefs)
        commentJlInstruction(  opcode, ea, block.id, blockDefs)
        commentJleInstruction( opcode, ea, block.id, blockDefs)
        commentJgInstruction(  opcode, ea, block.id, blockDefs)
        commentJgeInstruction( opcode, ea, block.id, blockDefs)
        commentJaInstruction(  opcode, ea, block.id, blockDefs)
        commentJbInstruction(  opcode, ea, block.id, blockDefs)
        commentJbeInstruction( opcode, ea, block.id, blockDefs)
        commentJnbInstruction( opcode, ea, block.id, blockDefs)
        sameBlockLoopDetect(opcode, ea, block) # call afer all jumps
        commentCallInstruction(opcode, ea, block.id, blockDefs)
        commentMovInstruction( opcode, ea, block.id, blockDefs)
        commentMovsxInstruction(opcode, ea, block.id, blockDefs)
        commentMovzxInstruction(opcode, ea, block.id, blockDefs)
        commentLeaInstruction( opcode, ea, block.id, blockDefs)
        commentIMulInstruction(opcode, ea, block.id, blockDefs)
        commentCdqInstruction( opcode, ea, block.id, blockDefs)
        commentIDivInstruction(opcode, ea, block.id, blockDefs)
        commentLeaveInstruction(opcode, ea, block.id, blockDefs)
        commentRetNInstruction(opcode, ea, block.id, blockDefs)
        commentShlInstruction( opcode, ea, block.id, blockDefs)
        commentShrInstruction( opcode, ea, block.id, blockDefs)
        commentSarInstruction( opcode, ea, block.id, blockDefs)
        commentSetnlInstruction(opcode, ea, block.id, blockDefs)
        commentStosdInstruction(opcode, ea, block.id, blockDefs)
        ea = idc.NextHead(ea)


                                

def getInstructions(block, blockDefPath={}):
    blockDefs = [blockDefPath]
    if(blockToDefs.get(block.id) == None):
        blockToDefs[block.id] = blockDefPath.copy()
    
    getInstructionsBase(block, blockDefs)
    return blockToDefs[block.id]



def bfs(basicBlockStart):
    queue = []
    visited = set([])
    queue.append( basicBlockStart )
    while ( len(queue) > 0 ):
        vBlock = queue.pop(0); #front pop
        if(vBlock.id not in visited):
            getInstructionsPrevWalkBack(vBlock)
            visited.add(vBlock.id);
            for succBlock in vBlock.succs():
                queue.append(succBlock)
                print succBlock.id


def dfs(basicBlockStart, basicBlockEnd):
    stack = []
    visited = set([])
    stack.append( (basicBlockStart,[]) );
    while(len(stack) > 0):
        sTop = stack.pop();
        vBlock = sTop[0]
        path = sTop[1][:]
        if(vBlock.id not in visited):
            visited.add(vBlock.id);
            path.append(vBlock)
            for succBlock in vBlock.succs():
                if(succBlock.id == basicBlockEnd.id):
                    path.append(succBlock)
                    return path
                stack.append( (succBlock,path) )
    return None

def printPath(path):
    print map(lambda x: x.id, path)

def printPathSet(keyA, keyB, pathSet):
    pathIds = []
    for path in pathSet:
        pathIds.append(map(lambda x: x.id, path))
    print "block {} reaches block {} through path {}".format(keyA,keyB, pathIds)

def printDom(id, blocks):
    blockIds = []
    map(lambda block: blockIds.append(block.id), blocks)
    print "block {} dominated by {}".format(id, blockIds)

def printDoms(domLists):
    for id, blocks in domLists.iteritems():
        printDom(id, blocks)

def printPaths(paths):
    for keyA, valueA in paths.iteritems():
        for keyB, valueB in valueA.iteritems():
            printPathSet(keyA, keyB, valueB)

def getDominatedBy(func, isStrictDom=False):
    domDict = {}
    global reachablePaths
    flow = idaapi.FlowChart(func)
    for domByBlock in flow:
        domList = []
        for domBlock in flow:
            if(reachablePaths.get(domBlock.id) == None):
                continue
            elif(((not isStrictDom) and domBlock.id == domByBlock.id) or
               (reachablePaths[domBlock.id].get(domByBlock.id) !=None)):
                domList.append(domBlock)

        domDict[domByBlock.id] = domList

    printDoms(domDict)
    return domDict

def AllPathsRecurse(basicBlockStart, basicBlockEnd, visited, path, reachablePaths):
    visited.add(basicBlockStart.id)
    path.append(basicBlockStart)
    if(basicBlockStart.id == basicBlockEnd.id):
        if(reachablePaths.get(path[0].id) == None):
            reachablePaths[path[0].id] = {path[-1].id : [path[:]]}
        elif(reachablePaths[path[0].id].get(path[-1].id) == None):
            reachablePaths[path[0].id][path[-1].id] = [path[:]];
        else:
            reachablePaths[path[0].id][path[-1].id].append(path[:])
        #print map(lambda x: x.id, path)
    else:
        for succBlock in basicBlockStart.succs():
            if(succBlock.id not in visited):
                AllPathsRecurse(succBlock, basicBlockEnd, visited, path, reachablePaths)
    path.pop()
    visited.remove(basicBlockStart.id)


def AllPaths(basicBlockStart, basicBlockEnd):
    visited = set([])
    path = []
    global reachablePaths
    AllPathsRecurse(basicBlockStart, basicBlockEnd, visited, path, reachablePaths)

def dfsAllPath(blockA, blockB):
    global reachablePaths
    path = dfs(blockA,blockB)
    if(path):
        if(reachablePaths.get(blockA.id) == None):
            reachablePaths[blockA.id] = {blockB.id : [path]}
        elif(reachablePaths[blockA.id].get(blockB.id) == None):
            reachablePaths[blockA.id][blockB.id] = [path];
        else:
            reachablePaths[blockA.id][blockB.id].append(path)


def genReachableBasicBlocks(func,useAllPaths=True):
    global reachablePaths
    global shadowStack
    reachablePaths = {}
    flow = idaapi.FlowChart(func, flags=idaapi.FC_PREDS)
    if flow.size == 1:
        blockDef = initBlock()
        shadowStack = [["0",""]]
        for block in flow:
            blockDef = getInstructions(block, blockDef);
            WriteEAs(block)
    else:
        for blockA in flow:
            for blockB in flow:
                if(blockA.id == blockB.id):
                    continue
                else:
                    if(useAllPaths):
                        AllPaths(blockA, blockB)
                    else:
                        dfsAllPath(blockA, blockB)
        uniquePaths = genUniqueReachablePaths(flow)
        if not useAllPaths:
            uniquePaths = uniquePaths + getUniqueList(genLoops(flow))
        walkReachablePaths(flow, uniquePaths)
        for block in flow:
            WriteEAs(block)


def getUniqueSets(l):
    l2 = l[:]
    pathIds = []
    for path in l:
        pathIds.append(map(lambda x: x.id, path))
    pathIdsToBlocksMap = {}
    for index in range(len(pathIds)):
        pathIdsToBlocksMap["{}".format(pathIds[index])] = l[index]
    pathIdsCopy = pathIds[:]
    #print pathIdsToBlocksMap
    for m in pathIds:
        for n in pathIds:
            if set(m).issubset(set(n)) and m != n:
                pathIdsCopy.remove(m)
                l2.remove(pathIdsToBlocksMap["{}".format(m)])
                break
    #print pathIdsCopy
    return l2

def sublist(a, b) :
    return b in [a[i:len(b)+i] for i in xrange(len(a))]

def getUniqueList(l):
    l2 = l[:]
    pathIds = []
    for path in l:
        pathIds.append(map(lambda x: x.id, path))
    pathIdsToBlocksMap = {}
    for index in range(len(pathIds)):
        pathIdsToBlocksMap["{}".format(pathIds[index])] = l[index]
    pathIdsCopy = pathIds[:]
    removedSet = set([])
    for m in pathIds:
        for n in pathIds:
            key = "{}".format(m)
            if key not in removedSet and sublist(n,m) and m != n:
                pathIdsCopy.remove(m)
                removedSet.add(key)
                #print "removing {}".format(key)
                l2.remove(pathIdsToBlocksMap[key])
                break;
    return l2

def genUniqueReachablePaths(flow):
    paths = []
    #print flow.size
    #print reachablePaths.keys()
    #for block in flow:
    #    print "block id: {}".format(block.id)
    for i in range(flow.size-1):
    #   print "i: {}".format(i)
        if reachablePaths.get(i) == None:
            continue
    #    print "path at i: {}".format(reachablePaths.get(i).keys())
        for j in range(flow.size):
    #        print "j: {}".format(j)
            if(reachablePaths[i].get(j) != None):
                paths = paths + reachablePaths[i].get(j)
    #print paths
    #print len(paths)
    #for path in paths:
    #    printPath(path)
    uniquePath = getUniqueList(paths)
    #print len(uniquePath)
    #for path in uniquePath:
    #    printPath(path)
    #print "_______________"
    return uniquePath

def getLoopStart(path):
    for blockIndex in range(len(path)-1):
        if path[blockIndex].endEA > path[blockIndex + 1].startEA:
            return path[blockIndex:]

def walkReachablePaths(flow, uniquePaths):
    visited = set([])
    blockToDefs = {}
    blockToUses = {}
    global shadowStack
    global callAgain
    registerBlockMap = {}
    eaToComputedValue = {}
    loopPathSet = set([])
    for path in uniquePaths:
        blockDefs = initBlock()
        shadowStack = [["0",""]]
        initRegisterValues()
        #printPath(path)
        loopBlock = None
        ids = map(lambda x: x.id, path)
        for prevBlock in path[0].preds():
            if prevBlock.id in ids:
                loopBlock = prevBlock
                #print "loop found block {}".format(loopBlock.id)
                break
    
        if loopBlock != None:
            loopPath = getLoopStart(path)
            loopPathHash = "{}".format(map(lambda x: x.id, loopPath));
            if loopPathHash not in loopPathSet:
                loopPathSet.add(loopPathHash)
                loopPaths = []
                for p in uniquePaths:
                    if loopPath[0] == p[-1]:
                        loopPaths.append(p[:-1] + loopPath)
                for lp in loopPaths:
                    for block in lp:
                        blockDefs = getInstructions(block,blockDefs)
        else:
            for block in path:
                #print "Shadow stack start size: {}".format(len(shadowStack))
                #print "block id: {}".format(block.id)
                blockDefs = getInstructions(block,blockDefs)
                #print "Shadow stack start size: {}".format(len(shadowStack))
        if(callAgain):
            blockDefs = getInstructions(block,blockDefs)
            #print "called block a second time"
            callAgain = False

def genLoops(flow):
    dist = {}
    next = {}
    warshawlAlgo(flow, dist, next)
    return pathReconstruction(flow, dist, next)

infinity = sys.maxsize
def warshawlAlgo(flow, dist, next):
    minValue = 1
    for i in range(flow.size):
        for j in range(flow.size):
            if dist.get(i) == None:
                dist[i] = {j: infinity }
            else:
                dist[i][j] =infinity
            if next.get(i) == None:
                next[i] = {j: None }
            else:
                next[i][j] = None

    for i in range(flow.size):
        block = flow[i]
        for succBlock in block.succs():
            dist[block.id][succBlock.id] = minValue
            next[block.id][succBlock.id] = succBlock

    for i in range(flow.size): # set diags to 0
        dist[i][i] = 0

    for i in flow:
        for j in flow:
            for k in flow:
                if (dist[i.id][j.id] > dist[i.id][k.id] + dist[k.id][j.id]):
                    dist[i.id][j.id] = dist[i.id][k.id] + dist[k.id][j.id];
                    next[i.id][j.id] = next[i.id][k.id];

def pathReconstruction(flow, dist, next):
    seenPathCombos = set([])
    seenPaths = []
    for i in range(flow.size):
        vBlock = flow[i]
        for j in range(flow.size):
            uBlock = flow[j]
            if (dist[vBlock.id][uBlock.id] == infinity or #skip non-weighted path
                dist[uBlock.id][vBlock.id] == infinity or
                dist[vBlock.id][uBlock.id] == 0        or #skip [v][v]
                dist[uBlock.id][vBlock.id] == 0):
                    continue;
            vuPath = basicBlockPath(vBlock,uBlock,next)
            uvPath = basicBlockPath(uBlock,vBlock,next)
            mPath = mergePaths(vuPath, uvPath)
            pathHash = "{}".format(map(lambda x: x.id, mPath))
            if(pathHash not in seenPathCombos):
                #print pathHash
                seenPathCombos.add(pathHash)
                if(mPath[0] == mPath[-1]):
                    mPath.pop() #found a cycle make it a-cyclic
                addToSeenPaths = True
                for sp in seenPaths:
                    if areVecsPerms(sp,mPath):
                        addToSeenPaths = False
                if(addToSeenPaths):
                    #print(pathHash)
                    seenPaths.append(mPath)
    return seenPaths


def basicBlockPath(u,v, next):
    if next[u.id][v.id] == None:
        return []
    path = []
    p = u
    while(p.id != v.id):
        p = next[p.id][v.id]
        path.append(p)
    return path

def mergePaths(vuPath, uvPath):
    concatPath = []
    if len(vuPath) > 0:
       concatPath = vuPath[:]
    if len(uvPath) > 0:
       if concatPath[-1] == uvPath[0]:
            concatPath.pop()
       concatPath = concatPath + uvPath
    return concatPath
       
def areVecsPerms(path1, path2):
    if(len(path1) != len(path2)):
       return False
    for blockA in path1:
       bExists = False
       for blockB in path2:
            if(blockA.id == blockB.id):
                bExists = True
                break;
       if bExists == False:
            return False;
    return True











